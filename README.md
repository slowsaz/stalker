[![Rawsec's CyberSecurity Inventory](https://inventory.rawsec.ml/img/badges/Rawsec-inventoried-FF5050_flat.svg)](https://inventory.rawsec.ml/tools.html#Stalker)

## What's Stalker

Very simple and probably badly written OSINT tool that will allow you to quickly search through several websites for a single username.

It's aimed to save time and I wanted to keep it simple, so everything's in one single, messy .py file. I'll soon work on that to clean all of this a little bit.

## How to use it

First off, clone this repo :

``git clone https://gitlab.com/Pxmme/stalker``

If you do not have Python3 installed nor pip3, install them :

``apt-get update``

``apt-get install python3 python3-pip``

Then go into the dir and install requirements :

``cd stalker``

``pip3 install -r requirements.txt``

Type ./stalker -h : you're good to go!

## Ammunitions

Create bogus accounts and add them to ammunitions.py to enable this script to scan on those websites/apps too!

Currently supports Snapchat and Geekmemore.

Be aware that it leaves traces! For instance, Geekmemore users will know your bogus account checked them out!

## Usage

```
usage: stalker.py [-h] -e ENUMERATE [-v] [-f] [-c] TARGET

positional arguments:
  TARGET                The nickname of your target

optional arguments:
  -h, --help            show this help message and exit
  -e ENUMERATE, --enumerate ENUMERATE
                        Categories to scan (a = all, s = social, m = media, b
                        = blog, g = gaming)
  -v, --verbose         Enables verbosity in order to display URLs when
                        accounts are found
  -f, --firefox         Open each finding in a Firefox tab
  -c, --chromium        Open each finding in a Chromium tab

Example of use : ./stalker.py -v -e a pxmme1337
```

## Example

![example gif](https://s3.gifyu.com/images/gifee6de36ac08612a3.gif)
