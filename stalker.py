#!/usr/bin/python3

# IMPORT #
import requests
import sys
import argparse
import os
import time

# PARSER #
parser = argparse.ArgumentParser(epilog='Example of use : ./stalker.py -v -e a pxmme1337')
parser.add_argument("TARGET", help="The nickname of your target")
parser.add_argument("-e","--enumerate", help="Categories to scan (a = all, s = social, m = media, b = blog, g = gaming)",required=True)
parser.add_argument("-v","--verbose", help="Enables verbosity in order to display URLs when accounts are found",action="store_true")
parser.add_argument("-f","--firefox", help="Open each finding in a Firefox tab",action="store_true")
parser.add_argument("-c","--chromium", help="Open each finding in a Chromium tab",action="store_true")
args = parser.parse_args()

# STATIC VARIABLES #

target = str(args.TARGET)
if "." in target:
	print("[x] No '.' allowed in nickname.")
	exit()
file = open('./version.txt','r')
version = file.read()[:5]

# RECONSTITUTION OF COMMAND FOR AUTO UPDATE

command = "./stalker.py "
if args.firefox:
	command += "-f "
if args.chromium:
	command += "-c "
command  += "-e "+args.enumerate+"x "+target

# DICTIONARIES #
# ENTRIES :
# [ HANDLE , BASEURL, APPEND BEFORE NICKNAME, APPEND AFTER NICKNAME, SUCCESS HTTP CODE, (optional) SPECIAL STRING TO LOOK FOR IN RESPONSE TEXT FOR FAILURE]
# EXAMPLE :
# ['Twitter','https://twitter.com/','','',200]
# https://twitter.com/nickname
# if r.status_code = 200 : SUCCESS
social = [
['Twitter','https://twitter.com/','','',200],
['Facebook','https://www.facebook.com/','','',200],
['Instagram','https://www.instagram.com/','','',200],
['Jeuxvideo.com','http://www.jeuxvideo.com/','profil/','',200],
['Reddit','https://www.reddit.com/','user/','',200],
['Github','https://github.com/','','',200],
['Gitlab','https://gitlab.com/','','',200,'offers free unlimited (private) repositories'],
['Ello','https://ello.co/','','',200],
['Trip Advisor','https://www.tripadvisor.fr/','Profile/','',200],
['Paypal','https://www.paypal.me/','','',200,'Sorry'],
['Disqus','https://disqus.com/','by/','',200],
['VK','https://vk.com/','','',200],
['Badoo','https://badoo.com/','profile/','',200],
['Telegram','https://telegram.me/','','',200,'tgme_icon_user'],
]
media = [
['9gag','https://9gag.com/','u/','',200],
['Youtube','https://www.youtube.com/','user/','',200],
['Pinterest','https://www.pinterest.fr/','','',200],
['Vimeo','https://vimeo.com/','','',200],
['Dailymotion','https://www.dailymotion.com/','','',200],
['Spotify','https://open.spotify.com/','user/','',200],
['Imgur','https://imgur.com/','user/','',200,'Zoinks! You\'ve taken a wrong turn.'],
['Deviant Art','https://www.deviantart.com/','','',200],
['SoundCloud','https://soundcloud.com/','','',200],
['Twitch','https://m.twitch.tv/','','/profile',200],
['Photobucket','https://smg.photobucket.com/','user/','/library',200],
['Patreon','https://www.patreon.com/','','',200],
['Gravatar','https://en.gravatar.com/','','',200],
['Society6','https://society6.com/','','',200],
]
blog = [
['Blogger','http://','','.blogspot.com',200],
['Wordpress','https://','','.wordpress.com',200,'exist'],
['Tumblr','https://','','.tumblr.com',200],
['About Me','https://about.me/','','',200],
['Medium','https://medium.com/','@','',200],
['Skyrock','https://','','.skyrock.com',200],
['My Space','https://myspace.com/','','',200],
]
gaming = [
['Ankama', 'https://account.ankama.com/','fr/','',200],
['League of Legends (Europe West)','https://euw.op.gg/','summoner/userName=','',200,'SummonerNotFound'],
['League of Legends (North America)','https://na.op.gg/','summoner/userName=','',200,'SummonerNotFound'],
['League of Legends (Europe Nordic & East)','https://eune.op.gg/','summoner/userName=','',200,'SummonerNotFound'],
['League of Legends (Korea)','https://www.op.gg/','summoner/userName=','',200,'SummonerNotFound'],
['League of Legends (Oceania)','https://oce.op.gg/','summoner/userName=','',200,'SummonerNotFound'],
['League of Legends (Russia)','https://ru.op.gg/','summoner/userName=','',200,'SummonerNotFound'],
['Steam','https://steamcommunity.com/','id/','',200,'error_ctn'],
['Star Citizen','https://robertsspaceindustries.com/','citizens/','',200],
]
specials = ['Wordpress',
'League of Legends (Europe West)',
'League of Legends (North America)',
'League of Legends (Europe Nordic & East)',
'League of Legends (Korea)','League of Legends (Oceania)',
'League of Legends (Russia)',
'Steam',
'Imgur',
'Gitlab',
'Paypal',
'Telegram'
]

# RICE

if "x" not in args.enumerate:
	print(' .           ..         .           .       .           .           .')
	print('      .         .            .          .       .')
	print('            .         ..xxxxxxxxxx....               .       .             .')
	print('    .             MWMWMWWMWMWMWMWMWMWMWMWMW                       .')
	print('              IIIIMWMWMWMWMWMWMWMWMWMWMWMWMWMttii:        .           .')
	print(' .      IIYVVXMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWxx...         .           .')
	print('     IWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMx..')
	print('   IIWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMWMx..        .')
	print('    ""MWMWMWMWMWM"""""""".  .:..   ."""""MWMWMWMWMWMWMWMWMWMWMWMWMWti.')
	print(' .     ""   . `  .: . :. : .  . :.  .  . . .  """"MWMWMWMWMWMWMWMWMWMWMWMWMti=')
	print('        . .   :` . :   .  .\'.\' \'....xxxxx...,\'. \'   \' ."""YWMWMWMWMWMWMWMWMWMW+')
	print('     ; . ` .  . : . .\' :  . ..XXXXXXXXXXXXXXXXXXXXx.    `     . "YWMWMWMWMWMWMW')
	print('.    .  .  .    . .   .  ..XXXXXXXXWWWWWWWWWWWWWWWWXXXX.  .     .     """""""')
	print('        \' :  : . : .  ...XXXXXWWW"   W88N88@888888WWWWWXX.   .   .       . .')
	print('   . \' .    . :   ...XXXXXXWWW"    M88N88GGGGGG888^8M "WMBX.          .   ..  :')
	print('         :     ..XXXXXXXXWWW"     M88888WWRWWWMW8oo88M   WWMX.     .    :    .')
	print('           "XXXXXXXXXXXXWW"       WN8888WWWWW  W8@@@8M    BMBRX.         .  : :')
	print('  .       XXXXXXXX=MMWW":  .      W8N888WWWWWWWW88888W      XRBRXX.  .       .')
	print('     ....  ""XXXXXMM::::. .        W8@889WWWWWM8@8N8W      . . :RRXx.    .')
	print('         ``...\'\'\'  MMM::.:.  .      W888N89999888@8W      . . ::::"RXV    .  :')
	print(' .       ..\'\'\'\'\'      MMMm::.  .      WW888N88888WW     .  . mmMMMMMRXx')
	print('      ..\' .            ""MMmm .  .       WWWWWWW   . :. :,miMM"""  : ""`    .')
	print('   .                .       ""MMMMmm . .  .  .   ._,mMMMM"""  :  \' .  :')
	print('               .                  ""MMMMMMMMMMMMM""" .  : . \'   .        .')
	print('          .              .     .    .                      .         .')
	print('.                                         .          .         .')
	print()
	print()
	print('                    					"Got my eyes on you."')
	print()
	print('                    					Running version '+version)
	print()
	print()
	print('                         Written by Pxmme (@pxmme1337)')
	print('                         Ascii art made by "Zach & Owen"')
	print()
else:
	print("[+] Update completed. Let's go.")
	print()

# FUNCTIONS

def check(network, specials):
	counter = 0
	for site in network:
		if network[counter][0] in specials:
			check_in(counter, network, specials)
		else:
			r = requests.get(network[counter][1]+network[counter][2]+target+network[counter][3])
			if r.status_code == network[counter][4] and network[counter][0] not in specials:
				print("[+] "+network[counter][0]+" account found!")
				if args.verbose:
					print("[-] Verbose mode : "+network[counter][1]+network[counter][2]+target+network[counter][3])
				if args.firefox:
					os.popen("firefox "+network[counter][1]+network[counter][2]+target+network[counter][3]+" >/dev/null 2>&1")
				if args.chromium:
					os.popen("chromium --no-sandbox "+network[counter][1]+network[counter][2]+target+network[counter][3]+" >/dev/null 2>&1")
		counter += 1

def check_in(counter, network, specials):
	inreq = requests.get(network[counter][1]+network[counter][2]+target+network[counter][3])
	if network[counter][5] not in inreq.text:
		print("[+] "+network[counter][0]+" account found!")
		if args.verbose:
			print("[-] Verbose mode : "+network[counter][1]+network[counter][2]+target+network[counter][3])
		if args.firefox:
			os.popen("firefox "+network[counter][1]+network[counter][2]+target+network[counter][3]+ ">/dev/null 2>&1")
		if args.chromium:
			os.popen("chromium --no-sandbox "+network[counter][1]+network[counter][2]+target+network[counter][3]+" >/dev/null 2>&1")

def version_check(version):
	answered = False
	r = requests.get('https://gitlab.com/Pxmme/stalker/raw/master/version.txt')
	if version != r.text[:5]:
		print("[!] Wow! Hold on a minute.")
		print("[!] There is a new version of this tool available.")
		print("[!] You're currently running version "+version)
		print("[!] Version "+r.text[:5]+" is available.")
		answer = input(str("[?] Do you want to update it now? [Y/N]\n"))
		while answered == False:
			if answer == "Y":
				os.system("git pull &> /dev/null")
				print("[+] Updating...")
				os.system("/bin/bash -c \"cd " + os.getcwd() + " && " + command + "\"")
				exit()
			elif answer == "N":
				print("[!] Alright.")
				answered = True
			else:
				answer = input(str("[!] Please answer with Y for YES or N for NO."))

def start_browsers(args):
	if args.firefox:
		print("[+] Starting Firefox...")
		os.popen("firefox")
		time.sleep(5)
	if args.chromium:
		print("[+] Starting Chromium...")
		os.popen("chromium --no-sandbox")
		time.sleep(5)

# VERSION CHECK

version_check(version)

# SHALL WE START SOME BROWSERS?

start_browsers(args)

# LET'S GO!

# SOCIAL
if "a" in args.enumerate or "s" in args.enumerate:
	check(social,specials)
# MEDIA
if "a" in args.enumerate or "m" in args.enumerate:
	check(media,specials)
# BLOG
if "a" in args.enumerate or "b" in args.enumerate:
	check(blog,specials)
# GAMING
if "a" in args.enumerate or "g" in args.enumerate:
	check(gaming,specials)

# THE END

print("[+] We're done here.")
